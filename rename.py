from os import listdir, rename, getcwd
from os.path import join, isfile
import sys


def showHelp():
    print("USAGE:")
    print("python3 rename.py -p pics/ -i 1 -v")
    print()
    print("-i %int% -  start index")
    print("-p %str% -  path to folder with files")
    print("-v       -  verbose")
    print("-h       -  help")


def renameFiles(path, index, verbose):
    print("Path:        {}".format(path))
    print("Start index: {}".format(index))

    renameInfo = ""
    for s in sorted(listdir(path)):
        ss = s.split(".")
        if (isfile(join(path, s))  and  ss[len(ss)-1].upper() == 'NEF'):
            newName = 'DSC_' + str(index).zfill(4) + '.NEF'
            renameInfo = s + " -> " + newName
            if (verbose):
                print(renameInfo)
            rename(join(path, s), join(path, newName))
            index += 1

    print("Last:        {}\nDone!".format(renameInfo))


# MAIN
path = ""
index = -1
verbose = False

i = 1;
shouldRename = True
while (i < len(sys.argv)):
    if (sys.argv[i].lower() == "-h"):
        showHelp()
        shouldRename = False
        break
    elif (sys.argv[i].lower() == "-p"):
        i += 1
        path = sys.argv[i]
        i += 1
    elif (sys.argv[i].lower() == "-i"):
        i += 1
        index = int(sys.argv[i])
        i += 1
    elif (sys.argv[i].lower() == "-v"):
        verbose = True
        i += 1
    else:
        print("Unknown input")
        showHelp()
        shouldRename = False
        break

if (shouldRename):
    if (index == -1):
        index = int(input("Enter start index for new sequence: "))
    if (path == ""):
        path = getcwd()
    renameFiles(path, index, verbose)