from os import listdir, rename, getcwd
from os.path import join, isfile

i = 0
startIndex = int(input("Enter start index for new sequence: "))
for s in sorted(listdir(getcwd())):
    ss = s.split(".")
    if (isfile(join(getcwd(), s))  and  ss[len(ss)-1].upper() == 'NEF'):
        newName = 'DSC_' + str(startIndex+i).zfill(4) + '.NEF'
        print(s + " -> " + newName)
        rename(join(getcwd(), s), join(getcwd(), newName))
        i += 1