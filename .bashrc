### My aliases
alias wca="git ls-files | grep -E '^.*.(js|css|jade)$' | xargs wc -l"
alias wcjs="git ls-files | grep -E '^.*.(js)$' | xargs wc -l"
alias rld=". ~/.bashrc"

alias gs="git status"
alias gl="git lg"
alias gla="git lga"
alias gitPrune="git remote prune origin | grep -E '\[pruned\] [A-Za-z0-9-]+\/([A-Za-z0-9\/-]+)' -o | sed 's/\[pruned\] [A-Za-z0-9-]*\///' | xargs git branch -d"
alias makeAlpha="nrun makeAlphaVersion"
alias makeRelease="nrun makeReleaseVersion"
alias revertPackageVersion="nrun revertPackageVersion"
alias npmlk="npm install --package-lock-only"
alias fvpn="sudo openconnect vpn.fabit.ru --user=smakaev"
